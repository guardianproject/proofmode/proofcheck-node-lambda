FROM node:16-slim as builder
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
ARG APP_DIR=/opt/proofcheck-node-lambda/
RUN mkdir -p ${APP_DIR}
COPY . ${APP_DIR}
RUN chown -R node ${APP_DIR}
RUN ls -al ${APP_DIR}
USER node
WORKDIR ${APP_DIR}
RUN npm run config
RUN npm install --force
RUN npm run build

FROM public.ecr.aws/lambda/nodejs:16
ARG BUILD_DATE
ARG VERSION
LABEL maintainer="Darren Clarke <darren@redaranj.com>"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.version=$VERSION
ARG APP_DIR=/opt/proofcheck-node-lamba/
ENV APP_DIR ${APP_DIR}
COPY --from=builder ${APP_DIR}package.json ${LAMBDA_TASK_ROOT}/package.json
COPY --from=builder ${APP_DIR}node_modules ${LAMBDA_TASK_ROOT}/node_modules
COPY --from=builder ${APP_DIR}build/index.js ${LAMBDA_TASK_ROOT}/index.js
COPY --from=builder ${APP_DIR}pyodide ${LAMBDA_TASK_ROOT}/pyodide
CMD [ "index.handler" ]

import { checkCID } from "@guardianproject/proofcheck";

export const handler = async (event) => {
  const { cid } = event;
  const final = await checkCID(cid);

  return {
    status: 200,
    statusDescription: "OK",
    body: JSON.stringify(final),
  };
};

import { handler } from "./index";

const executeHandler = async () => {
  const result = await handler({
    cid: "bafybeibdm2vrrcmh35itn6idzb4oaw3vbo3kaxosdzfxiecwytf3hitveq/proofmode-36fcf0dcb4357dcb-2022-07-26-14-11-28CDT.zip",
  });
  console.log({ result });
};

executeHandler();
